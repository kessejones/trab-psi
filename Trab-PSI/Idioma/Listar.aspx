﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MaterPage.Master" AutoEventWireup="true" CodeBehind="Listar.aspx.cs" Inherits="Trab_PSI.Idioma.Listar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>LISTA DE IDIOMAS</h3>
    <asp:GridView runat="server" ID="gridIdiomas" AutoGenerateColumns="false" OnRowCommand="gridIdiomas_RowCommand">
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:HiddenField ID="idIdioma" Value='<%# Eval("idIdioma") %>' runat="server"/>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="idioma" />
            <asp:ButtonField ButtonType="Button" Text="Deletar" CommandName="idiomaDeletar"/>
        </Columns>
    </asp:GridView>

    <asp:Label Text="text" ID="lblLog" runat="server" />
</asp:Content>
