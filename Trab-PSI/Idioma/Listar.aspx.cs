﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Trab_PSI.Idioma
{
    public partial class Listar : System.Web.UI.Page
    {
        private Trab_PSI.dsCinemaTableAdapters.idiomasTableAdapter taIdiomas;

        protected void Page_Load(object sender, EventArgs e)
        {
            taIdiomas = new dsCinemaTableAdapters.idiomasTableAdapter();
            CarregarGrid();
        }

        protected void gridIdiomas_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("idiomaDeletar"))
            {
                int indexRow = Convert.ToInt32(e.CommandArgument);

                HiddenField hiddenField = gridIdiomas.Rows[indexRow].Cells[0].FindControl("idIdioma") as HiddenField;
                int idIdioma = Convert.ToInt32(hiddenField.Value);
                taIdiomas.deleteIdioma(idIdioma);
                CarregarGrid();
            }
        }

        private void CarregarGrid()
        {
            gridIdiomas.DataSource = taIdiomas.getIdiomas();
            gridIdiomas.DataBind();
        }
    }
}