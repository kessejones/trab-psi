﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Trab_PSI.Idioma
{
    public partial class Cadastrar : System.Web.UI.Page
    {
        private Trab_PSI.dsCinemaTableAdapters.idiomasTableAdapter taIdioma;

        protected void Page_Load(object sender, EventArgs e)
        {
            taIdioma = new dsCinemaTableAdapters.idiomasTableAdapter();
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            int ?retorno = 0;
            taIdioma.insertIdioma(txtIdioma.Text, ref retorno);
        }
    }
}