﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Trab_PSI.FilmeAtor
{
    public partial class Cadastrar : System.Web.UI.Page
    {
        private Trab_PSI.dsCinemaTableAdapters.filmesAtoresTableAdapter taFilmeAtor;
        private Trab_PSI.dsCinemaTableAdapters.filmesTableAdapter taFilmes;
        private Trab_PSI.dsCinemaTableAdapters.atoresTableAdapter taAtor;
       
        protected void Page_Load(object sender, EventArgs e)
        {
            taFilmeAtor = new dsCinemaTableAdapters.filmesAtoresTableAdapter();
            

            /*
             * Retorna os filmes e preenche o DropDownList sFilme
             */
            taFilmes = new dsCinemaTableAdapters.filmesTableAdapter();

            sFilme.DataSource = taFilmes.getFilmes();
            sFilme.DataValueField = "idFilme";
            sFilme.DataTextField = "titulo";
            sFilme.DataBind();

            /*
             * Retorna os filmes e preenche o DropDownList sFilme
             */
            taAtor = new dsCinemaTableAdapters.atoresTableAdapter();

            sAtor.DataSource = taAtor.getAtores();
            sAtor.DataValueField = "idAtor";
            sAtor.DataTextField = "nome";
            sAtor.DataBind();

        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            taFilmeAtor.insertFilmeAtor(Convert.ToInt32(sFilme.SelectedValue), Convert.ToInt32(sAtor.SelectedValue));
        }
    }
}