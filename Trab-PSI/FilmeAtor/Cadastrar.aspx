﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MaterPage.Master" AutoEventWireup="true" CodeBehind="Cadastrar.aspx.cs" Inherits="Trab_PSI.FilmeAtor.Cadastrar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label Text="Filme" runat="server"/>
    <asp:DropDownList id="sFilme" runat="server">
        
    </asp:DropDownList>
    
    <asp:Label Text="Ator" runat="server"/>
    <asp:DropDownList id="sAtor" runat="server">
        
    </asp:DropDownList>

    <asp:Button Text="Cadastrar" runat="server" OnClick="btnSalvar_Click" />
</asp:Content>
