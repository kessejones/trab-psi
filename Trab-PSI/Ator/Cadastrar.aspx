﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MaterPage.Master" AutoEventWireup="true" CodeBehind="Cadastrar.aspx.cs" Inherits="Trab_PSI.Ator.Cadastrar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>CADASTRAR ATOR</h3>
    <asp:Label Text="Nome" runat="server"/>
    <asp:TextBox runat="server" ID="txtNome"/>
    <asp:Label Text="Sobrenome" runat="server" />
    <asp:TextBox runat="server" ID="txtSobrenome"/>
    <asp:Button Text="Salvar" runat="server" ID="btnSalvar" OnClick="btnSalvar_Click"/>
</asp:Content>
