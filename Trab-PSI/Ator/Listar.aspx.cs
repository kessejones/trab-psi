﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Trab_PSI.Ator
{
    public partial class Listar : System.Web.UI.Page
    {
        private Trab_PSI.dsCinemaTableAdapters.atoresTableAdapter taAtores;
        protected void Page_Load(object sender, EventArgs e)
        {
            taAtores = new dsCinemaTableAdapters.atoresTableAdapter();
            CarregarGrid();
        }

        protected void gridAtores_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if(e.CommandName.Equals("atorDeletar"))
            {
                int indexRow = Convert.ToInt32(e.CommandArgument);

                HiddenField hiddenField = gridAtores.Rows[indexRow].Cells[0].FindControl("idAtor") as HiddenField;
                int idAtor = Convert.ToInt32(hiddenField.Value);
                taAtores.deleteAtor(idAtor);
                CarregarGrid();
            }
        }

        private void CarregarGrid()
        {
            gridAtores.DataSource = taAtores.getAtores();
            gridAtores.DataBind();
        }
    }
}