﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MaterPage.Master" AutoEventWireup="true" CodeBehind="Listar.aspx.cs" Inherits="Trab_PSI.Ator.Listar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>LISTA DE ATORES</h3>
    <asp:GridView runat="server" ID="gridAtores" AutoGenerateColumns="false" OnRowCommand="gridAtores_RowCommand">
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:HiddenField ID="idAtor" Value='<%# Eval("idAtor") %>' runat="server"/>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="nome" />
            <asp:BoundField DataField="sobrenome" />
            <asp:ButtonField ButtonType="Button" Text="Deletar" CommandName="atorDeletar"/>
        </Columns>
    </asp:GridView>

    <asp:Label Text="text" ID="lblLog" runat="server" />
</asp:Content>
