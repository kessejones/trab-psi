﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Trab_PSI.Ator;

namespace Trab_PSI.Ator
{
    public partial class Cadastrar : System.Web.UI.Page
    {

        private Trab_PSI.dsCinemaTableAdapters.atoresTableAdapter taAtor;

        protected void Page_Load(object sender, EventArgs e)
        {
            taAtor = new Trab_PSI.dsCinemaTableAdapters.atoresTableAdapter();
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            taAtor.insertAtor(txtNome.Text, txtSobrenome.Text);
        }
    }
}