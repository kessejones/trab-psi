﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Trab_PSI.Filme
{
    public partial class Listar : System.Web.UI.Page
    {
        private Trab_PSI.dsCinemaTableAdapters.filmesTableAdapter taFilmes;

        protected void Page_Load(object sender, EventArgs e)
        {
            taFilmes = new dsCinemaTableAdapters.filmesTableAdapter();
            CarregarGrid();
        }

        protected void gridFilmes_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("filmeDeletar"))
            {
                int indexRow = Convert.ToInt32(e.CommandArgument);

                HiddenField hiddenField = gridFilmes.Rows[indexRow].Cells[0].FindControl("idAtor") as HiddenField;
                int idFilme = Convert.ToInt32(hiddenField.Value);
                taFilmes.deleteFilme(idFilme);
                CarregarGrid();
            }
        }

        private void CarregarGrid()
        {
            gridFilmes.DataSource = taFilmes.getFilmes();
            gridFilmes.DataBind();
        }
    }
}