﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Trab_PSI.Filme
{
    public partial class Cadastrar : System.Web.UI.Page
    {
        private Trab_PSI.dsCinemaTableAdapters.filmesTableAdapter taFilmes;
        private Trab_PSI.dsCinemaTableAdapters.idiomasTableAdapter taIdioma;
        private Trab_PSI.dsCinemaTableAdapters.categoriasTableAdapter taCategoria;
        private Trab_PSI.dsCinemaTableAdapters.classificacoesTableAdapter taClassificacao;

        protected void Page_Load(object sender, EventArgs e)
        {
            taFilmes = new dsCinemaTableAdapters.filmesTableAdapter();

            /*
             * Retorna os idiomas e preenche o DropDownList sIdioma
             */
            taIdioma = new Trab_PSI.dsCinemaTableAdapters.idiomasTableAdapter();

            sIdioma.DataSource = taIdioma.getIdiomas();
            sIdioma.DataValueField = "idIdioma";
            sIdioma.DataTextField = "idioma";
            sIdioma.DataBind();

            /*
             * Retorna as categorias e preenche o DropDownList sCategoria
             */
            taCategoria = new Trab_PSI.dsCinemaTableAdapters.categoriasTableAdapter();

            sCategoria.DataSource = taCategoria.getCategorias();
            sCategoria.DataValueField = "idCategoria";
            sCategoria.DataTextField = "categoria";
            sCategoria.DataBind();

            /*
             * Retorna as classificações e preenche o DropDownList sClassificacao
             */
            taClassificacao = new Trab_PSI.dsCinemaTableAdapters.classificacoesTableAdapter();

            sClassificacao.DataSource = taClassificacao.getClassificacoes();
            sClassificacao.DataValueField = "idClassificacao";
            sClassificacao.DataTextField = "classificacao";
            sClassificacao.DataBind();
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            taFilmes.insertFilme(txtTitulo.Text, txtDescricao.Text, Convert.ToInt32(txtAno.Text), Convert.ToInt32(sIdioma.SelectedValue), Convert.ToInt32(sClassificacao.SelectedValue), Convert.ToInt32(sCategoria.SelectedValue));
        }


    }
}