﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MaterPage.Master" AutoEventWireup="true" CodeBehind="Cadastrar.aspx.cs" Inherits="Trab_PSI.Filme.Cadastrar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>CADASTRAR FILME</h3>
    <asp:Label Text="Titulo" runat="server"/>
    <asp:TextBox runat="server" ID="txtTitulo"/>
    <asp:Label Text="Descrição" runat="server" />
    <asp:TextBox runat="server" ID="txtDescricao"/>
    <asp:Label Text="Ano de Lançamento" runat="server" />
    <asp:TextBox runat="server" ID="txtAno"/>
    
    <asp:Label Text="Idioma" runat="server"/>
    <asp:DropDownList id="sIdioma" runat="server">
        
    </asp:DropDownList>
    
    <asp:Label Text="Categoria" runat="server"/>
    <asp:DropDownList id="sCategoria" runat="server">
        
    </asp:DropDownList>

    <asp:Label Text="Classificação" runat="server"/>
    <asp:DropDownList id="sClassificacao" runat="server">
        
    </asp:DropDownList>
    <asp:Button Text="Salvar" runat="server" ID="btnSalvar" OnClick="btnSalvar_Click"/>
</asp:Content>
