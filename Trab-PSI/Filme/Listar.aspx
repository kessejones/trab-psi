﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MaterPage.Master" AutoEventWireup="true" CodeBehind="Listar.aspx.cs" Inherits="Trab_PSI.Filme.Listar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>LISTA DE FILMES</h3>
    <asp:GridView runat="server" ID="gridFilmes" AutoGenerateColumns="false" OnRowCommand="gridFilmes_RowCommand">
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:HiddenField ID="idFilme" Value='<%# Eval("idFilme") %>' runat="server"/>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="titulo" />
            <asp:BoundField DataField="descricao" />
            <asp:BoundField DataField="anoLancamento" />
            <asp:ButtonField ButtonType="Button" Text="Deletar" CommandName="filmeDeletar"/>
        </Columns>
    </asp:GridView>

    <asp:Label Text="text" ID="lblLog" runat="server" />
</asp:Content>
